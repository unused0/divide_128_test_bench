#!/bin/sh
CC1=' /opt/oracle/developerstudio12.6/bin/suncc -Qy -xO1 -m64 -xlibmil -xCC -mt -xlibmopt -fno-semantic-interposition -xprefetch=auto -xprefetch_level=3 -g3 -U__STRICT_ANSI__ -D_GNU_SOURCE -DNEED_128'
CC5=' /opt/oracle/developerstudio12.6/bin/suncc -Qy -xO5 -m64 -xlibmil -xCC -mt -xlibmopt -fno-semantic-interposition -xprefetch=auto -xprefetch_level=3 -g3 -U__STRICT_ANSI__ -D_GNU_SOURCE -DNEED_128'

$CC1 -c scp.c -o scp.o
$CC1 -c dps8_math128.c -o dps8_math128.o -I../simh
$CC1 -S dps8_math128.c -o dps8_math128_1.s -I../simh
$CC1 scp.o dps8_math128.o -o dps81
./dps81
$CC5 -c scp.c -o scp.o
$CC5 -c dps8_math128.c -o dps8_math128.o -I../simh
$CC5 -S dps8_math128.c -o dps8_math128_5.s -I../simh
$CC5 scp.o dps8_math128.o -o dps85
./dps85






