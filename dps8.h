/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 6965b612-f62e-11ec-a432-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2012 Dave Jordan
 * Copyright (c) 2013-2018 Charles Anthony
 * Copyright (c) 2016 Jean-Michel Merliot
 * Copyright (c) 2021-2022 The DPS8M Development Team
 *
 * All rights reserved.
 *
 * This software is made available under the terms of the ICU
 * License, version 1.8.1 or later.  For more details, see the
 * LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#ifndef DPS8_H
# define DPS8_H

# include <stdio.h>
# include <stdbool.h>
# include <errno.h>
# include <inttypes.h>
# include <sys/stat.h>
# include <sys/time.h>
# include <setjmp.h>  // for setjmp/longjmp used by interrupts & faults

# if (defined(__APPLE__) && defined(__MACH__)) || defined(__ANDROID__)
#  include <libgen.h>  // needed for OS/X and Android
# endif

typedef struct { uint64_t h; uint64_t l; } __uint128_t;
typedef struct { int64_t h;  uint64_t l; }  __int128_t;
#  define construct_128(h, l) ((uint128) { (h), (l) })
#  define construct_s128(h, l) ((int128) { (h), (l) })


# include <stdlib.h>


typedef unsigned int    uint32;

typedef signed long long        t_int64;
typedef unsigned long long      t_uint64;

typedef t_uint64    uint64;
typedef t_int64     int64;

/* Data types */

typedef __uint128_t uint128;
typedef __int128_t  int128;


typedef unsigned int uint;      // efficient unsigned int, at least 32 bits

# include "dps8_math128.h"

#  define MASK63          0x7FFFFFFFFFFFFFFF
#  define MASK64          0xFFFFFFFFFFFFFFFF
# define SIGN64          ((uint64)1U << 63)
# define MEM_SIZE_MAX    (1U << PASIZE)           /* maximum memory */



#endif // ifdef DPS8_H
