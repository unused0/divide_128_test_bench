#define sim_printf printf
/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * SPDX-License-Identifier: Multics
 * scspell-id: 928c0e86-f62e-11ec-ab5c-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2016 Jean-Michel Merliot
 * Copyright (c) 2021-2022 The DPS8M Development Team
 *
 * All rights reserved.
 *
 * This software is made available under the terms of the ICU
 * License, version 1.8.1 or later.  For more details, see the
 * LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 *
 * This source file may contain code comments that adapt, include, and/or
 * incorporate Multics program code and/or documentation distributed under
 * the Multics License.  In the event of any discrepancy between code
 * comments herein and the original Multics materials, the original Multics
 * materials should be considered authoritative unless otherwise noted.
 * For more details and historical background, see the LICENSE.md file at
 * the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

# define DPS8_MATH128
# include "dps8.h"
# include "dps8_math128.h"

static int32_t nlz (unsigned x)
  {
    unsigned y;
    int n;

    n = 32;
    y = x >>16; if (y != 0) {n = n -16;x = y;}
    y = x >> 8; if (y != 0) {n = n - 8;x = y;}
    y = x >> 4; if (y != 0) {n = n - 4;x = y;}
    y = x >> 2; if (y != 0) {n = n - 2;x = y;}
    y = x >> 1; if (y != 0) return n - 2;
    return n - (int) x;
 }

int divmnu (uint16_t q[], uint16_t r[],
            const uint16_t u[], const uint16_t v[],
            int m, int n)
  {

    const uint32_t b = 65536;   // Number base (16 bits).
    uint16_t *un, *vn;          // Normalized form of u, v.
    unsigned qhat;              // Estimated quotient digit.
    unsigned rhat;              // A remainder.
    unsigned p;                 // Product of two digits.
    int s, i, j, t, k;

    if (m < n || n <= 0 || v[n-1] == 0)
      return 1;                 // Return if invalid param.

    // Take care of the case of a single-digit span
    if (n == 1)
      {
        k = 0;
        for (j = m - 1; j >= 0; j--)
          {
            q[j] = (uint16_t) (((unsigned int) k*b + u[j])/v[0]);    // divisor here.
            k = (int) (((unsigned int) k*b + u[j]) - q[j]*v[0]);
          }
        if (r != NULL) r[0] = (uint16_t) k;
        return 0;
      }

    // Normalize by shifting v left just enough so that
    // its high-order bit is on, and shift u left the
    // same amount. We may have to append a high-order
    // digit on the dividend; we do that unconditionally.

    s = nlz (v[n-1]) - 16;      // 0 <= s <= 16.
    vn = (uint16_t *) alloca (2*n);
    for (i = n - 1; i > 0; i--)
      vn[i] = (uint16_t) (v[i] << s) | (uint16_t) (v[i-1] >> (16-s));
    vn[0] = (uint16_t) (v[0] << s);

    un = (uint16_t *) alloca(2*(m + 1));
    un[m] = u[m-1] >> (16-s);
    for (i = m - 1; i > 0; i--)
      un[i] = (uint16_t) (u[i] << s) | (uint16_t) (u[i-1] >> (16-s));
    un[0] = (uint16_t) (u[0] << s);
    for (j = m - n; j >= 0; j--)
      {       // Main loop.
        // Compute estimate qhat of q[j].
        qhat = (un[j+n]*b + un[j+n-1])/vn[n-1];
        rhat = (un[j+n]*b + un[j+n-1])%vn[n-1];
again:
        if (qhat >= b || (unsigned)qhat*(unsigned long long)vn[n-2] > b*rhat + un[j+n-2])
          {
            qhat = qhat - 1;
            rhat = rhat + vn[n-1];
            if (rhat < b) goto again;
          }

        // Multiply and subtract.
        k = 0;
        for (i = 0; i < n; i++)
          {
            p = (unsigned)qhat*(unsigned long long)vn[i];
            t = (int32_t) un[i+j] - k - (int32_t) (p & 0xFFFF);
            un[i+j] = (uint16_t) t;
            k = (int) (p >> 16) - (t >> 16);
          }
        t = un[j+n] - k;
        un[j+n] = (uint16_t) t;

        q[j] = (uint16_t) qhat;            // Store quotient digit.
        if (t < 0)
          {            // If we subtracted too
            q[j] = q[j] - 1;       // much, add back.
            k = 0;
            for (i = 0; i < n; i++)
              {
                t = un[i+j] + vn[i] + k;
                un[i+j] = (uint16_t) t;
                k = t >> 16;
               }
             un[j+n] = (uint16_t) (un[j+n] + k);
          }
      } // End j.
    // If the caller wants the remainder, denormalize
    // it and pass it back.
    if (r != NULL)
      {
        for (i = 0; i < n; i++)
          r[i] = (uint16_t) (un[i] >> s) | (uint16_t) (un[i+1] << (16-s));
      }
    return 0;
  }



// Note: divisor is < 2^16
uint128 divide_128 (uint128 a, uint128 b, uint128 * remp)
  {
    bool dbg = a.h == 0 && a.l == 010144 && b.h == 0 && b.l == 0240735354000;
//sim_printf ("%o %o %o %o\r\n", a.h, a.l, b.h, b.l);
    const int m = 8;
    const int n = 8;
    uint16_t q[m], u[m], v[n];
    u[0] = (uint16_t)  a.l;
    u[1] = (uint16_t) (a.l >> 16);
    u[2] = (uint16_t) (a.l >> 32);
    u[3] = (uint16_t) (a.l >> 48);
    u[4] = (uint16_t)  a.h;
    u[5] = (uint16_t) (a.h >> 16);
    u[6] = (uint16_t) (a.h >> 32);
    u[7] = (uint16_t) (a.h >> 48);

    v[0] = (uint16_t)  b.l;
    v[1] = (uint16_t) (b.l >> 16);
    v[2] = (uint16_t) (b.l >> 32);
    v[3] = (uint16_t) (b.l >> 48);
    v[4] = (uint16_t)  b.h;
    v[5] = (uint16_t) (b.h >> 16);
    v[6] = (uint16_t) (b.h >> 32);
    v[7] = (uint16_t) (b.h >> 48);

    int normlen;
    for (normlen = 8; normlen > 0; normlen --)
      if (v [normlen - 1])
        break;
    uint16_t r [8] = { 8 * 0 };
    divmnu (q, remp ? r : NULL, u, v, m, normlen);
    if (dbg) sim_printf ("quot %06o %06o %06o %06o  %06o %06o %06o %06o\r\n", r[7], r[6], r[5], r[4], r[3], r[2], r[1], r[0]);
    if (remp)
      {
        * remp =  construct_128 (
       (((uint64_t) r [7]) << 48) |
       (((uint64_t) r [6]) << 32) |
       (((uint64_t) r [5]) << 16) |
       (((uint64_t) r [4]) <<  0),
       (((uint64_t) r [3]) << 48) |
       (((uint64_t) r [2]) << 32) |
       (((uint64_t) r [1]) << 16) |
       (((uint64_t) r [0]) <<  0));
      }
    return construct_128 (
       (((uint64_t) q [7]) << 48) |
       (((uint64_t) q [6]) << 32) |
       (((uint64_t) q [5]) << 16) |
       (((uint64_t) q [4]) <<  0),
       (((uint64_t) q [3]) << 48) |
       (((uint64_t) q [2]) << 32) |
       (((uint64_t) q [1]) << 16) |
       (((uint64_t) q [0]) <<  0));
  }


int test_math128 (void)
  {
    sim_printf ("testing divide_128...\r\n");
    uint128 dFrac, zFrac;
    uint128 quot, remainder;
    dFrac.h = 0;
    dFrac.l = 0240735354000;
    zFrac.h = 0;
    zFrac.l = 010144;
    quot = divide_128 (zFrac, dFrac, & remainder);
    sim_printf ("quot %012llo %012llo\n", quot.h, quot.l);
    sim_printf ("remainder %012llo %012llo\n", remainder.h, remainder.l);

    return 0;
  }
